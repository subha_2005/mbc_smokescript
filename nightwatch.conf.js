var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
//var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data = require('./TestResources/MBCSmokeData1');
const osHomedir = require('os-homedir');
var path = osHomedir();
var homepath = path.replace(new RegExp('\\' + path.sep, 'g'), '/');
sauce_labs_username = "monica-c";
sauce_labs_access_key = "425a0af4-1689-4e89-8396-54c2e5095152";

require('nightwatch-cucumber')({
    //supportFiles: ['../utils/TestExecListener.js'],
    stepTimeout: 2900000,
    defaultTimeoutInterval: 40000,
    nightwatchClientAsParameter: true
});
var fnRandomNumber = function (no_of_digits) {
    var max = 9, min = 0, str="", i;
    for(i=0; i < no_of_digits; i++){
        str = str + Math.floor(Math.random() * (max - min) + min);
    }
    return str;
};

var RandomPort =fnRandomNumber(4);

module.exports = {

    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "repository",
    globals_path: "./TestResources/MBCSmokeData1",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: RandomPort,
        platform: 'Windows 10',
        cli_args: {
            "webdriver.chrome.driver" :chromedriver.path,
            'webdriver.gecko.driver': firefoxDriver.path
        }
    },
    test_settings: {
        default : {
            launch_url: "http://localhost",
            page_objects_path : 'repository',
            selenium_host: "127.0.0.1",
            selenium_port: RandomPort,
            silent : true,
            disable_colors: false,
            screenshots: {
                enabled: false,
                on_failure: true,
                on_error: true,
                path: 'Screenshots'
            },

            globals: {
                waitForConditionTimeout: 1000000
            },
            desiredCapabilities: {
                browserName: data.BrowserInTest,
                javascriptEnabled : true,
                acceptSslCerts : true
            }
        },
    }



}
