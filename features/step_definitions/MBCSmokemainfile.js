var data = require('./../../TestResources/MBCSmokeData1');
var action = require('./../../Keywords');
var URL;


module.exports = function () {
    this.Given(/^User launches "([^"]*)" application and enters credentials$/, function (browser, loginType) {
        var URL;
        var execEnv = data["TestingEnvironment"];
        URL = data[loginType][execEnv].URL;
        browser.url(URL);
        action.initializePageObjects(browser, function () {
            action.isDisplayed('LoginPage|Username', function () {
                action.setText('LoginPage|Username', data[loginType][execEnv].UserName);
                action.isDisplayed('LoginPage|Password', function () {
                    action.setText('LoginPage|Password', data[loginType][execEnv].Password);
                    action.isDisplayed('LoginPage|SubmitButton', function () {
                        action.performClick('LoginPage|SubmitButton', function () {


                            })
                        })
                    })
                })
            })
        })



    this.When(/^User clicks on Forms and Documents subtab under Health and Benefits nav tab$/, function (browser) {
        browser.pause(data.shortWait);
        action.isDisplayed('LoginPage|MenuButton', function () {
            action.performClick('LoginPage|MenuButton', function () {
                action.isDisplayed('LoginPage|HeaderText', function () {
                    action.verifyText('LoginPage|HeaderText', "FORMS & DOCUMENTS");
                    action.isDisplayed('LoginPage|HealthnBenefits', function () {
                        action.performClick('LoginPage|HealthnBenefits', function () {
                        })
                    })
                })
            })
        });
    });
};