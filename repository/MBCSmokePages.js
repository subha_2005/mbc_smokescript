module.exports = {
        sections: {
            LoginPage: {
                selector: 'body',
                elements: {
                    Username: {locateStrategy: 'xpath',selector: "//input[@id='usernameId']"},
                    UsernameMAS: {locateStrategy: 'xpath',selector: "//input[@id='username']"},
                    Password: {locateStrategy: 'xpath',selector: "//input[@id='passwordId']"},
                    PasswordMAS: {locateStrategy: 'xpath',selector: "//input[@id='password']"},
                    SubmitButton: {locateStrategy: 'xpath',selector: "(//input[@type='submit'])[1]"},
                    SubmitButtonMAS: {locateStrategy: 'xpath',selector: "//button[.='Login']"},
                    DashboardTab:{locateStrategy:'xpath', selector:"//span[contains(text(),'Dashboard')]"},
                    MenuButton: {locateStrategy: 'xpath',selector: "//span[text()='Menu']"},
                    HealthnBenefits: {locateStrategy: 'xpath',selector: "(//span[.='Health & Benefits'])[1]"},
                    CompleteLifeEventButton: {locateStrategy: 'xpath', selector: "(//span[@class='ng-binding'])[2]"},
                    OpenEnrollmentButton: {locateStrategy: 'xpath',selector: "//span[.='Open Enrollment']"},
                    AnnualEnrollmentButton: {locateStrategy: 'xpath', selector: "(//span[@class='ng-binding'])[2]"},
                    LifeEventPageLink: {locateStrategy: 'xpath', selector: "(//span[@class='ng-binding ng-scope'])[6]"},
                    LogoutButton: {locateStrategy: 'xpath',selector: "//a[text()='Logout']"},
                    Dependentslink:{locateStrategy: 'xpath',selector: "//span[.='Dependents']"},
                    DepPresent:{locateStrategy: 'xpath',selector: "//span[.='Dep A']"},
                    Button:{locateStrategy: 'xpath', selector:"//*[@id=\"globalMegaNav\"]/div[1]/div/a[2]/span"},
                    HeaderText:{locateStrategy: 'xpath', selector:"(//div[@class='ng-scope']/h6)[3]"}

                }
            },

        FormsPage: {
            selector: 'body',
            elements: {
                FormsTab: {locateStrategy: 'xpath',selector: "//a[text()='Forms']"},
                FormsHeader:{locateStrategy: 'xpath',selector: "//h1[text()='Forms']"},
                NoForms:{locateStrategy: 'xpath',selector: "//div[@class='ng-scope']/p"},
                Formstable:{locateStrategy: 'xpath',selector: "//div[@class='row row-table well-inner full ng-scope'][1]"},
                FormsDoc: {locateStrategy: 'xpath',selector: "(//div[@class='category-link col-sm-1'])[1]/div"}
            }
        },
        DocsPage: {
            selector: 'body',
            elements: {
                DocsTab: {locateStrategy: 'xpath', selector: "//a[text()='Documents']"},
                DocsHeader: {locateStrategy: 'xpath', selector: "//h1[text()='Documents']"},
                NoDocs: {selector: "body > div.page > div:nth-child(4) > div > div > div:nth-child(3) > div > div > div.ng-scope > p"},
                Docstable: {selector: "body > div.page > div:nth-child(4) > div > div > div:nth-child(3) > div > div > div.ng-scope > div > div.well.collapse.in"},
                Doc: {selector: "body > div.page > div:nth-child(4) > div > div > div:nth-child(3) > div > div > div.ng-scope > div > div.well.collapse.in > div.forms-documents-category > div > div.category-info.col-sm-10 > div > div:nth-child(1) > div.category-link.col-sm-1 > div > span > a > i:nth-child(3)"}
            }
        },
        LEPage: {
            selector: 'body',
            elements: {
                GetStartedLink: {locateStrategy: 'xpath', selector: "(//a[@class='icon-link ng-scope'])[1]"},
                LEDate: {locateStrategy: 'xpath', selector: "//input[@type='text']"},
                GetStartedButton: {locateStrategy: 'xpath', selector: "//button[@class='allcaps btn btn-secondary start']"}
            }
        },
        LifeEventsPage: {
            selector: 'body',
            elements: {
                GetStartedTab: {locateStrategy: 'xpath', selector: "(//a[@class='ng-scope ng-binding'])[1]"},
                GetStartedLink: {locateStrategy: 'xpath', selector: "(//a[@bindonce='contentPromise'])[1]"},
                NextButton:{locateStrategy: 'xpath', selector: "//button[.='NEXT']"},
                WhosCoveredTab:{locateStrategy: 'xpath', selector: "(//a[@bindonce='contentPromise'])[2]"},
                //WhosCoveredTab:{locateStrategy: 'xpath', selector: "(//a[@class='ng-scope ng-binding'])[5]"},
                AddDependentButton:{locateStrategy: 'xpath', selector: "(//a[@class='btn btn-secondary allcaps'])[1]"},
                ContinueButtonforDepAddn:{locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary'])[2]"},
                FName:{locateStrategy: 'xpath', selector: "//input[@name='FirstName']"},
                LName:{locateStrategy: 'xpath', selector: "//input[@name='LastName']"},
                DOB:{locateStrategy: 'xpath', selector: "//input[@name='BirthDate']"},
                SSN:{locateStrategy: 'xpath', selector: "//input[@name='SSN']"},
                Gender:{locateStrategy: 'xpath', selector: "(//button[@name='Gender'])[1]"},
                RelationDropdown:{locateStrategy:'xpath', selector: "(//span[@class='filter-option pull-left'])[1]"},
                Child:{locateStrategy:'xpath', selector: "(//span[@class='text'])[2]"},
                Spouse:{locateStrategy:'xpath', selector: "(//span[@class='text'])[8]"},
                LiveAtSameAddress:{locateStrategy:'xpath', selector: "(//button[@type='button'])[9]"},
                AgreeButtonWCP:{locateStrategy:'xpath', selector: "(//button[@type='button'])[12]"},
                SaveButton:{locateStrategy:'xpath', selector: "//button[.='Save']"},
                QuickSelectMed:{locateStrategy:'xpath', selector: "(//button[@type='button'])[6]"},
                QuickSelectDen:{locateStrategy:'xpath', selector: "(//button[@type='button'])[7]"},
                QuickSelectVis:{locateStrategy:'xpath', selector: "(//button[@type='button'])[8]"},
                ContinueButtonWCP:{locateStrategy: 'xpath', selector: "//span[.='Continue']"},
                MyInfoNoRadioButton:{locateStrategy: 'xpath', selector: "(//button[@class='btn btn-radio ng-pristine ng-binding ng-empty ng-invalid ng-invalid-required ng-touched'])"},
                UpdateButton: {locateStrategy: 'xpath', selector: "//button[.='Update']"},
                ContinueButtoninMyInfo: {locateStrategy: 'xpath', selector: "//span[.='Continue']"},
                ContinueButtoninHMFP: {locateStrategy: 'xpath', selector: "//button[.='Skip']"},
                NextButtoninYSP: {locateStrategy: 'xpath', selector: "(//span[.='Next'])[1]"},
                AddToCartMedPlan: {locateStrategy: 'xpath', selector: "(//a[@class='btn btn-primary'])[1]"},
                ShoppingCartMedPage: {locateStrategy: 'xpath', selector: "(//p[@class='ng-scope ng-binding'])[3]"},
                ShoppingCartButton: {locateStrategy: 'xpath', selector: "//button[@id='shoppingCartBtn']"},
                NextButtoninMedPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninSupPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninDenPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninVisPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninSAPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninLifePage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninDisPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninProPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                NextButtoninAddPage: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next'])[1]"},
                CheckboxinRYCP: {locateStrategy: 'xpath', selector: "//button[.='Yes, I accept.']"},
                SubmitButtoninRYCP: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary btn-next ng-scope'])[2]"},
                CheckListButton: {locateStrategy: 'xpath', selector: "(//button[@class='btn btn-primary'])[2]"},
                ReturnToHomeButton: {locateStrategy: 'xpath', selector: "//a[.='Return to Home']"}
            }
        }


    }};