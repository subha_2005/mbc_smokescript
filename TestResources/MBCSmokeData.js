module.exports =
    {
        PageSet: {
            PageRepos: process.env.PageName
        },

        //TestingEnvironment: 'Dev',
        TestingEnvironment: 'QAI3',
        //TestingEnvironment: 'QAF'
        //TestingEnvironment: 'CIT',
        //TestingEnvironment: 'CSO',
        //TestingEnvironment: 'Prod',
        Formserror:'There are no forms available to display.',
        Docserror: 'There are no documents available to display.',
        buttonle:'COMPLETE LIFE EVENT',
        buttonoe:'Open Enrollment',
        buttonae:'ANNUAL ENROLLMENT',
        EditinMyInfo:'Edit',
        Date:'07/09/2018',
        FullName:'Dep A',
        FName:'Dep',
        LName:'A',
        DOB:'05/06/2000',
        SSN:'201520158',
        wait: '5000',
        longWait:'10000',
        Loginwait: '30000',
        URLDev: 'https://ben-dev3.mercerbenefitscentral.com/?clientid=DVCLIENTC&employeeId=117301055',
        URLQAI: 'https://auth-qai.mercerbenefitscentral.com/mbcqa3/login.tpz',
        URLQAF: 'https://ben-qaf3.mercerbenefitscentral.com',
        URLCIT: 'https://auth-cit.mercer.com/mbctra/login',
        URLCSO: 'https://auth-cso.mercerbenefitscentral.com/mbctra/login.tpz',
        URLProd: 'https://auth.mercerbenefitscentral.com/mbctra/login.tpz',
        URLQaf: 'https://ben-qaf3.mercerbenefitscentral.com/?clientid=FNCLIENTC&employeeid=117301055',

        //QAI
        //UsernameQAI: 'testmbc33184',
        //PasswordQAI: 'Test0001',
        //QAF
        UsernamQAF: 'fnclientc1055',
        PasswordQAF: 'Test0001',
        //CIT
        UsernameCIT: 'citmas@mail.com',
        PasswordCIT: 'Mercer01!',
        //CSO
        UsernameCSO: 'MBCTRA0006',
        PasswordCSO: 'MBCTRAPASS2',
        //Prod
        UsernameProd: 'rachana.gn@mercer.com',
        PasswordProd: 'Test@468',

        UsernameQAI: 'testmbc33196',
        PasswordQAI: 'test0001'

    }