module.exports =
    {
        PageSet: {
            PageRepos: process.env.PageName
        },

        TestingEnvironment: 'CIT',
        BrowserInTest: 'chrome',
        shortWait: 5000,
        longWait: 10000,
        LoginWait: 30000,
        MAS: {

            QAI: {
                URL: 'https://auth-qa.mercer.com/mbcqa5/login',
                UserName: 'subha.kannan@mercer.com',
                Password: 'Mercer02*'

            },
            CIT: {

                URL: 'https://auth-cit.mercer.com/mbctra/login',
                UserName: 'citmas@mail.com',
                Password: 'Mercer01!'
            },
            CSO: {

                URL: 'https://auth-cso.mercer.com/mbctra/login',
                UserName: 'mohsin.railiwale@mercer.com ',
                Password: 'Test@468'
            },
            PROD: {

                URL: 'https://auth.mercer.com/mbctra/login',
                UserName: 'subha2.k@mail.com',
                Password: 'Test@468'

            }
        },

        MBC:

            {

                DEV3:
                    {
                        URL: 'https://ben-dev3.mercerbenefitscentral.com/?clientid=DVCLIENTC&employeeId=117301049'
                    },

                QAF:
                    {
                        URL: 'https://ben-qaf3.mercerbenefitscentral.com/?clientid=FNCLIENTC&employeeid=117301055'

                    },

                QAI3:
                    {
                        URL: 'https://auth-qai.mercerbenefitscentral.com/mbcqa3/login.tpz',
                        Username: 'testmbc33125',
                        Password: 'Test0001'
                    },

                CIT:
                    {
                        URL: 'https://auth-cit.mercerbenefitscentral.com/PBSPHL/login.tpz',
                        UserName: 'Hmk527181',
                        Password: 'Highmark2017'
                    }


            }
    };
